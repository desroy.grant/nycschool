//
//  HomeBaseViewController.swift
//  NYC School Challenge
//
//  Created by Desroy Grant on 4/9/20.
//  Copyright © 2020 Kadd Technology. All rights reserved.
//

import Foundation
import UIKit

class HomeBaseViewController : UIViewController{
    override func viewDidLoad() {
        super.viewDidLoad();
    }
    func getNavigationBarHeight() -> CGFloat{
        var height = CGFloat(0);
        if #available(iOS 13.0, *) {
            var statusBarHeight = CGFloat(0);
            let statusView = self.calcStatusBarView();
            if statusView != nil {
                statusBarHeight = statusView!.frame.size.height;
            }
            let navHeight = (self.navigationController?.navigationBar.frame.height ?? 0.0);
            height = statusBarHeight + navHeight;
        } else {
            let statusBarHeight = UIApplication.shared.statusBarFrame.height;
            if self.navigationController != nil {
                height = (self.navigationController?.navigationBar.frame.height)! + statusBarHeight;
            }else {
                height = statusBarHeight;
            }
        }
        return height;
    }
    func calcStatusBarView() -> UIView?{
        if #available(iOS 13.0, *) {
            let tag = 38482
            let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first

            if let statusBar = keyWindow?.viewWithTag(tag) {
                return statusBar
            } else {
                guard let statusBarFrame = keyWindow?.windowScene?.statusBarManager?.statusBarFrame else { return nil }
                let statusBarView = UIView(frame: statusBarFrame)
                statusBarView.tag = tag
                keyWindow?.addSubview(statusBarView)
                return statusBarView
            }
        }else{
            let statusBar = UIApplication.shared.value(forKey:"statusBar") as? UIView;
            return statusBar;
        }
    }
}
