//
//  TitleValueView.swift
//  NYC School Challenge
//
//  Created by Desroy Grant on 4/9/20.
//  Copyright © 2020 Kadd Technology. All rights reserved.
//

import Foundation
import UIKit
class TitleValueView: UIView {
    private var _titleControl:UILabel!;
    private var _valueControl:UILabel!;
    
    private let titleFont = UIFont(name: "HelveticaNeue", size: 14.5)
    private let valueFont = UIFont(name: "HelveticaNeue", size: 12)
    private let titleColor = UIColor.gray;
    private let valueColor = UIColor.black;
    private var _titleText:String!;
    private var _valueText:String!;
    init(title:String,value:String, frame:CGRect) {
        super.init(frame:frame);
        self._titleText = title;
        self._valueText = value;
        self.configControls();
    }
    override init(frame: CGRect) {
        super.init(frame: frame);
        self.configControls();
    }
    
    private func configControls(){
        let padding:CGFloat = 13;
        var startY:CGFloat = 2;
        let verticalSpace:CGFloat = 0;
        let controlWidth:CGFloat = self.frame.width - (padding*2);
        
        self._titleControl = UILabel(frame: CGRect(x: padding, y: startY, width: controlWidth, height: 19));
        self._titleControl.textColor = self.titleColor;
        
        self._titleControl.font = self.titleFont;
        self.addSubview(self._titleControl);
        
        startY += self._titleControl.frame.height + verticalSpace;
        let valueHeight = self.frame.height - startY;
        self._valueControl = UILabel(frame: CGRect(x: padding, y: startY, width: controlWidth, height: valueHeight));
        self._valueControl.lineBreakMode = .byWordWrapping;
        self._valueControl.numberOfLines = 0;
        self._valueControl.font = self.valueFont;
        self._valueControl.textColor = self.valueColor;
        
        self._titleControl.text = self._titleText;
        self._valueControl.text = self._valueText;
        
        self.addSubview(self._valueControl);
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

