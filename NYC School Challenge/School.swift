//
//  School.swift
//  NYC School Challenge
//
//  Created by Desroy Grant on 4/9/20.
//  Copyright © 2020 Kadd Technology. All rights reserved.
//

import Foundation
class School {
    private var _dbn:String!;
    private var _name:String!;
    private var _boro:String!;
    private var _overview:String!;
    private var _school10thSeats:String!;
    private var _academicopportunities1:String!;
    private var _academicopportunities2:String!;
    private var _ellPrograms:String!;
    private var _neighborhood:String!;
    private var _buildingCode:String!;
    private var _location:String!;
    private var _phoneNumber:String!;
    private var _faxNumber:String!;
    private var _email:String!;
    private var _website:String!;
    private var _subway:String!;
    private var _bus:String!;
    private var _grades2018:String!;
    private var _finalgrades:String!;
    private var _totalStudents:String!;
    private var _extracurricularActivities:String!;
    private var _schoolSports:String!;
    private var _attendanceRate:String!;
    private var _pctStuEnoughVariety:String!;
    private var _pctStuSafe:String!;
    private var _accessibilityDescription:String!;
    private var _directions1:String!;
    private var _requirement1:String!;
    private var _requirement2:String!;
    private var _requirement3:String!;
    private var _requirement4:String!;
    private var _requirement5:String!;
    private var _offerRate1:String!;
    private var _program1:String!;
    private var _code1:String!;
    private var _interest1:String!;
    private var _method1:String!;
    private var _seats9ge1:String!;
    private var _grade9gefilledflag1:String!;
    private var _grade9geapplicants1:String!;
    private var _seats101:String!;
    private var _admissionspriority11:String!;
    private var _admissionspriority21:String!;
    private var _admissionspriority31:String!;
    private var _grade9geapplicantsperseat1:String!;
    private var _grade9swdapplicantsperseat1:String!;
    private var _address:String!;
    private var _city:String!;
    private var _zip:String!;
    private var _stateCode:String!;
    private var _latitude:String!;
    private var _longitude:String!;
    private var _communityBoard:String!;
    private var _councilDistrict:String!;
    private var _censusTract:String!;
    private var _bin:String!;
    private var _bbl:String!;
    private var _nta:String!;
    private var _borough:String!;
    private var _numberOfSATTestTakers:String!;
    private var _criticalReadingAvgScore:String!;
    private var _mathAvgScore:String!;
    private var _writingAvgScore:String!;
    init(){
        
    }
    var DBN:String{
        get{
            return self._dbn;
        }set{
            self._dbn = newValue;
        }
    }
    var Name:String{
        get{
            return self._name;
        }set{
            self._name = newValue;
        }
    }
    var Boro:String{
        get{
            return self._boro;
        }set{
            self._boro = newValue;
        }
    }
    var Overview:String{
        get{
            return self._overview;
        }set{
            self._overview = newValue;
        }
    }
    var School10thSeats:String{
        get{
            return self._school10thSeats;
        }set{
            self._school10thSeats = newValue;
        }
    }
    var Academicopportunities1:String{
        get{
            return self._academicopportunities1;
        }set{
            self._academicopportunities1 = newValue;
        }
    }
    var Academicopportunities2:String{
        get{
            return self._academicopportunities2;
        }set{
            self._academicopportunities2 = newValue;
        }
    }
    var EllPrograms:String{
        get{
            return self._ellPrograms;
        }set{
            self._ellPrograms = newValue;
        }
    }
    var Neighborhood:String{
        get{
            return self._neighborhood;
        }set{
            self._neighborhood = newValue;
        }
    }
    var BuildingCode:String{
        get{
            return self._buildingCode;
        }set{
            self._buildingCode = newValue;
        }
    }
    var Location:String{
        get{
            return self._location;
        }set{
            self._location = newValue;
        }
    }
    var PhoneNumber:String{
        get{
            return self._phoneNumber;
        }set{
            self._phoneNumber = newValue;
        }
    }
    var FaxNumber:String{
        get{
            return self._faxNumber;
        }set{
            self._faxNumber = newValue;
        }
    }
    var Email:String{
        get{
            return self._email;
        }set{
            self._email = newValue;
        }
    }
    var Website:String{
        get{
            return self._website;
        }set{
            self._website = newValue;
        }
    }
    var Subway:String{
        get{
            return self._subway;
        }set{
            self._subway = newValue;
        }
    }
    var Bus:String{
        get{
            return self._bus;
        }set{
            self._bus = newValue;
        }
    }
    var Grades2018:String{
        get{
            return self._grades2018;
        }set{
            self._grades2018 = newValue;
        }
    }
    var Finalgrades:String{
        get{
            return self._finalgrades;
        }set{
            self._finalgrades = newValue;
        }
    }
    var TotalStudents:String{
        get{
            return self._totalStudents;
        }set{
            self._totalStudents = newValue;
        }
    }
    var ExtracurricularActivities:String{
        get{
            return self._extracurricularActivities;
        }set{
            self._extracurricularActivities = newValue;
        }
    }
    var SchoolSports:String{
        get{
            return self._schoolSports;
        }set{
            self._schoolSports = newValue;
        }
    }
    var AttendanceRate:String{
        get{
            return self._attendanceRate;
        }set{
            self._attendanceRate = newValue;
        }
    }
    var PctStuEnoughVariety:String{
        get{
            return self._pctStuEnoughVariety;
        }set{
            self._pctStuEnoughVariety = newValue;
        }
    }
    var PctStuSafe:String{
        get{
            return self._pctStuSafe;
        }set{
            self._pctStuSafe = newValue;
        }
    }
    var AccessibilityDescription:String{
        get{
            return self._accessibilityDescription;
        }set{
            self._accessibilityDescription = newValue;
        }
    }
    var Directions1:String{
        get{
            return self._directions1;
        }set{
            self._directions1 = newValue;
        }
    }
    var Requirement1:String{
        get{
            return self._requirement1;
        }set{
            self._requirement1 = newValue;
        }
    }
    var Requirement2:String{
        get{
            return self._requirement2;
        }set{
            self._requirement2 = newValue;
        }
    }
    var Requirement3:String{
        get{
            return self._requirement3;
        }set{
            self._requirement3 = newValue;
        }
    }
    var Requirement4:String{
        get{
            return self._requirement4;
        }set{
            self._requirement4 = newValue;
        }
    }
    var Requirement5:String{
        get{
            return self._requirement5;
        }set{
            self._requirement5 = newValue;
        }
    }
    var OfferRate1:String{
        get{
            return self._offerRate1;
        }set{
            self._offerRate1 = newValue;
        }
    }
    var Program1:String{
        get{
            return self._program1;
        }set{
            self._program1 = newValue;
        }
    }
    var Code1:String{
        get{
            return self._code1;
        }set{
            self._code1 = newValue;
        }
    }
    var Interest1:String{
        get{
            return self._interest1;
        }set{
            self._interest1 = newValue;
        }
    }
    var Method1:String{
        get{
            return self._method1;
        }set{
            self._method1 = newValue;
        }
    }
    var Seats9ge1:String{
        get{
            return self._seats9ge1;
        }set{
            self._seats9ge1 = newValue;
        }
    }
    var Grade9gefilledflag1:String{
        get{
            return self._grade9gefilledflag1;
        }set{
            self._grade9gefilledflag1 = newValue;
        }
    }
    var Grade9geapplicants1:String{
        get{
            return self._grade9geapplicants1;
        }set{
            self._grade9geapplicants1 = newValue;
        }
    }
    var Seats101:String{
        get{
            return self._seats101;
        }set{
            self._seats101 = newValue;
        }
    }
    var Admissionspriority11:String{
        get{
            return self._admissionspriority11;
        }set{
            self._admissionspriority11 = newValue;
        }
    }
    var Admissionspriority21:String{
        get{
            return self._admissionspriority21;
        }set{
            self._admissionspriority21 = newValue;
        }
    }
    var Admissionspriority31:String{
        get{
            return self._admissionspriority31;
        }set{
            self._admissionspriority31 = newValue;
        }
    }
    var Grade9geapplicantsperseat1:String{
        get{
            return self._grade9geapplicantsperseat1;
        }set{
            self._grade9geapplicantsperseat1 = newValue;
        }
    }
    var Grade9swdapplicantsperseat1:String{
        get{
            return self._grade9swdapplicantsperseat1;
        }set{
            self._grade9swdapplicantsperseat1 = newValue;
        }
    }
    func GetFullAddress() -> String {
        return String(format: "%@\n%@, %@ %@",self._address,self._city,self._stateCode,self._zip)
    }
    var Address:String{
        get{
            return self._address;
        }set{
            self._address = newValue;
        }
    }
    var City:String{
        get{
            return self._city;
        }set{
            self._city = newValue;
        }
    }
    var Zip:String{
        get{
            return self._zip;
        }set{
            self._zip = newValue;
        }
    }
    var StateCode:String{
        get{
            return self._stateCode;
        }set{
            self._stateCode = newValue;
        }
    }
    var Latitude:String{
        get{
            return self._latitude;
        }set{
            self._latitude = newValue;
        }
    }
    var Longitude:String{
        get{
            return self._longitude;
        }set{
            self._longitude = newValue;
        }
    }
    var CommunityBoard:String{
        get{
            return self._communityBoard;
        }set{
            self._communityBoard = newValue;
        }
    }
    var CouncilDistrict:String{
        get{
            return self._councilDistrict;
        }set{
            self._councilDistrict = newValue;
        }
    }
    var CensusTract:String{
        get{
            return self._censusTract;
        }set{
            self._censusTract = newValue;
        }
    }
    var Bin:String{
        get{
            return self._bin;
        }set{
            self._bin = newValue;
        }
    }
    var Bbl:String{
        get{
            return self._bbl;
        }set{
            self._bbl = newValue;
        }
    }
    var Nta:String{
        get{
            return self._nta;
        }set{
            self._nta = newValue;
        }
    }
    var Borough:String{
        get{
            return self._borough;
        }set{
            self._borough = newValue;
        }
    }
    var NumberOfSATTestTakers:String{
        get{
            return self._numberOfSATTestTakers;
        }set{
            self._numberOfSATTestTakers = newValue;
        }
    }
    var CriticalReadingAvgScore:String{
        get{
            return self._criticalReadingAvgScore;
        }set{
            self._criticalReadingAvgScore = newValue;
        }
    }
    var MathAvgScore:String{
        get{
            return self._mathAvgScore;
        }set{
            self._mathAvgScore = newValue;
        }
    }
    var WritingAvgScore:String{
        get{
            return self._writingAvgScore;
        }set{
            self._writingAvgScore = newValue;
        }
    }
}
