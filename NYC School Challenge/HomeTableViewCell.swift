//
//  HomeTableViewCell.swift
//  NYC School Challenge
//
//  Created by Desroy Grant on 4/9/20.
//  Copyright © 2020 Kadd Technology. All rights reserved.
//

import Foundation
import UIKit
class HomeTableViewCell: UITableViewCell{
    @IBOutlet var nameTitleControl: UILabel!
    @IBOutlet var nameValueControl: UILabel!
    @IBOutlet var phoneTitleControl: UILabel!
    @IBOutlet var phoneValueControl: UILabel!
    @IBOutlet var emailTitleControl: UILabel!
    @IBOutlet var emailValueControl: UILabel!
    @IBOutlet var addressTitleControl: UILabel!
    @IBOutlet var addressValueControl: UILabel!
    @IBOutlet var websiteTitleControl: UILabel!
    @IBOutlet var websiteValueControl: UILabel!
    
    private let titleFont = UIFont(name: "HelveticaNeue", size: 15)
    private let valueFont = UIFont(name: "HelveticaNeue", size: 13)
    private let titleColor = UIColor.black;
    private let valueColor = UIColor.gray;
    override func awakeFromNib() {
        super.awakeFromNib()
        separatorInset = UIEdgeInsets.zero;
        layoutMargins = UIEdgeInsets.zero;
        preservesSuperviewLayoutMargins = true;
        contentView.preservesSuperviewLayoutMargins = true;
        accessoryType = UITableViewCell.AccessoryType.disclosureIndicator;
        
        self.backgroundColor = .white;
        self.styleControls(titleControl: self.nameTitleControl, valueControl: self.nameValueControl)
        self.styleControls(titleControl: self.phoneTitleControl, valueControl: self.phoneValueControl)
        self.styleControls(titleControl: self.emailTitleControl, valueControl: self.emailValueControl)
        self.styleControls(titleControl: self.addressTitleControl, valueControl: self.addressValueControl)
        self.styleControls(titleControl: self.websiteTitleControl, valueControl: self.websiteValueControl)
    }
    
    private func styleControls(titleControl:UILabel, valueControl:UILabel){
        titleControl.font = self.titleFont;
        titleControl.textColor = self.titleColor;
        
        valueControl.font = self.valueFont;
        valueControl.textColor = self.valueColor;
    }
    
    func setModel(model:School){
        self.nameValueControl.text = model.Name;
        self.phoneValueControl.text = model.PhoneNumber;
        self.emailValueControl.text = model.Email;
        self.addressValueControl.text = model.GetFullAddress()
        self.websiteValueControl.text = model.Website;
    }
}
