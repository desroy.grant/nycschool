//
//  RequestManager.swift
//  NYC School Challenge
//
//  Created by Desroy Grant on 4/9/20.
//  Copyright © 2020 Kadd Technology. All rights reserved.
//

import Foundation
import UIKit

class RequestManager{
    init(){
    }
    func Send(urlPath:String,
              requestData:Data?,
              requestType:RequestType,
              completion:@escaping(Data?) -> Void,
              failure:@escaping (String) -> Void){
        
        let url = URL(string:urlPath);
        var request = URLRequest(url: url!, cachePolicy: .reloadIgnoringLocalCacheData);
        request.timeoutInterval = 60
        let config = URLSessionConfiguration.default
        config.urlCache = nil;
        let session = URLSession(configuration: config)
        
        if requestType == .GET {
            request.httpMethod = "GET"
        }else if requestType == .POST {
            request.httpMethod = "POST"
        }
        if requestData != nil{
            request.httpBody = requestData!
        }
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let task = session.dataTask(with:request) {data, response, error -> Void in
            var urlResponse:HTTPURLResponse?;
            if response != nil {
                urlResponse = response as? HTTPURLResponse
            }
            if error != nil {
                if response == nil {
                    print("Connection error")
                    failure("conerror");
                }else{
                    print("Connection Error. Error=",error!.localizedDescription)
                    failure("conerror");
                }
            }else if urlResponse != nil && urlResponse?.statusCode != 200 {
                var message:String;
                if data != nil {
                    message = String(data:data!, encoding:.utf8)!;
                }else{
                    message = "Unable to process your request at this time";
                }
                failure(message);
            }
            else{
                if data != nil {
                    completion(data!)
                }else{
                    completion(nil)
                }
            }
        };
        task.resume()
    }
}


