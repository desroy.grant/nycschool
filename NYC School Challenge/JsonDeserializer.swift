//
//  JsonDeserializer.swift
//  NYC School Challenge
//
//  Created by Desroy Grant on 4/9/20.
//  Copyright © 2020 Kadd Technology. All rights reserved.
//

import Foundation
class JsonDeserializer{
    init(){}
    func JsonToSchoolSATScores(data:Data?) -> [SchoolSatScore]?{
        if data == nil {
            return nil;
        }
        
        do{
            let rawRecords = try JSONSerialization.jsonObject(with: data!, options: []) as? [Any];
            if rawRecords == nil {
                return nil;
            }
            var results = [SchoolSatScore]();
            for rawData in rawRecords! {
                let lookup = rawData as? [String:Any];
                if lookup == nil {
                    continue;
                }
                let actLookup = lookup!;
                let item = SchoolSatScore();
                item.DBN = getJsonString(lookup:actLookup, key: "dbn");
                item.SchoolName = getJsonString(lookup:actLookup, key: "school_name");
                item.NumberOfSATTestTakers = getJsonString(lookup:actLookup, key: "num_of_sat_test_takers");
                item.CriticalReadingAvgScore = getJsonString(lookup:actLookup, key: "sat_critical_reading_avg_score");
                item.MathAvgScore = getJsonString(lookup:actLookup, key: "sat_math_avg_score");
                item.WritingAvgScore = getJsonString(lookup:actLookup, key: "sat_writing_avg_score");
                results.append(item);
            }
            return results;
        }catch {
            return nil;
        }
    }
    func JsonToSchools(data:Data?) -> [School]?{
        if data == nil {
            return nil;
        }
        
        do{
            let rawRecords = try JSONSerialization.jsonObject(with: data!, options: []) as? [Any];
            if rawRecords == nil {
                return nil;
            }
            var results = [School]();
            for rawData in rawRecords! {
                let lookup = rawData as? [String:Any];
                if lookup == nil {
                    continue;
                }
                let actLookup = lookup!;
                let item = School();
                item.DBN = getJsonString(lookup:actLookup, key: "dbn");
                item.Name = getJsonString(lookup:actLookup, key: "school_name");
                item.Boro = getJsonString(lookup:actLookup, key: "boro");
                item.Overview = getJsonString(lookup:actLookup, key: "overview_paragraph");
                item.School10thSeats = getJsonString(lookup:actLookup, key: "school_10th_seats");
                item.Academicopportunities1 = getJsonString(lookup:actLookup, key: "academicopportunities1");
                item.Academicopportunities2 = getJsonString(lookup:actLookup, key: "academicopportunities2");
                item.EllPrograms = getJsonString(lookup:actLookup, key: "ell_programs");
                item.Neighborhood = getJsonString(lookup:actLookup, key: "neighborhood");
                item.BuildingCode = getJsonString(lookup:actLookup, key: "building_code");
                item.Location = getJsonString(lookup:actLookup, key: "location");
                item.PhoneNumber = getJsonString(lookup:actLookup, key: "phone_number");
                item.FaxNumber = getJsonString(lookup:actLookup, key: "fax_number");
                item.Email = getJsonString(lookup:actLookup, key: "school_email");
                item.Website = getJsonString(lookup:actLookup, key: "website");
                item.Subway = getJsonString(lookup:actLookup, key: "subway");
                item.Bus = getJsonString(lookup: actLookup, key: "bus")
                item.Grades2018 = getJsonString(lookup:actLookup, key: "grades2018");
                item.Finalgrades = getJsonString(lookup:actLookup, key: "finalgrades");
                item.TotalStudents = getJsonString(lookup:actLookup, key: "total_students");
                item.ExtracurricularActivities = getJsonString(lookup:actLookup, key: "extracurricular_activities");
                item.SchoolSports = getJsonString(lookup:actLookup, key: "school_sports");
                item.AttendanceRate = getJsonString(lookup:actLookup, key: "attendance_rate");
                item.PctStuEnoughVariety = getJsonString(lookup:actLookup, key: "pct_stu_enough_variety");
                item.PctStuSafe = getJsonString(lookup:actLookup, key: "pct_stu_safe");
                item.AccessibilityDescription = getJsonString(lookup:actLookup, key: "school_accessibility_description");
                item.Directions1 = getJsonString(lookup:actLookup, key: "directions1");
                item.Requirement1 = getJsonString(lookup:actLookup, key: "requirement1_1");
                item.Requirement2 = getJsonString(lookup:actLookup, key: "requirement2_1");
                item.Requirement3 = getJsonString(lookup:actLookup, key: "requirement3_1");
                item.Requirement4 = getJsonString(lookup:actLookup, key: "requirement4_1");
                item.Requirement5 = getJsonString(lookup:actLookup, key: "requirement5_1");
                item.OfferRate1 = getJsonString(lookup:actLookup, key: "offer_rate1");
                item.Program1 = getJsonString(lookup:actLookup, key: "program1");
                item.Code1 = getJsonString(lookup:actLookup, key: "code1");
                item.Interest1 = getJsonString(lookup:actLookup, key: "interest1");
                item.Method1 = getJsonString(lookup:actLookup, key: "method1");
                item.Seats9ge1 = getJsonString(lookup:actLookup, key: "seats9ge1");
                item.Grade9gefilledflag1 = getJsonString(lookup:actLookup, key: "grade9gefilledflag1");
                item.Grade9geapplicants1 = getJsonString(lookup:actLookup, key: "grade9geapplicants1");
                item.Seats101 = getJsonString(lookup:actLookup, key: "seats101");
                item.Admissionspriority11 = getJsonString(lookup:actLookup, key: "admissionspriority11");
                item.Admissionspriority21 = getJsonString(lookup:actLookup, key: "admissionspriority21");
                item.Admissionspriority31 = getJsonString(lookup:actLookup, key: "admissionspriority31");
                item.Grade9geapplicantsperseat1 = getJsonString(lookup:actLookup, key: "grade9geapplicantsperseat1");
                item.Grade9swdapplicantsperseat1 = getJsonString(lookup:actLookup, key: "grade9swdapplicantsperseat1");
                item.Address = getJsonString(lookup:actLookup, key: "primary_address_line_1");
                item.City = getJsonString(lookup:actLookup, key: "city");
                item.Zip = getJsonString(lookup:actLookup, key: "zip");
                item.StateCode = getJsonString(lookup:actLookup, key: "state_code");
                item.Latitude = getJsonString(lookup:actLookup, key: "latitude");
                item.Longitude = getJsonString(lookup:actLookup, key: "longitude");
                item.CommunityBoard = getJsonString(lookup:actLookup, key: "community_board");
                item.CouncilDistrict = getJsonString(lookup:actLookup, key: "council_district");
                item.CensusTract = getJsonString(lookup:actLookup, key: "census_tract");
                item.Bin = getJsonString(lookup:actLookup, key: "bin");
                item.Bbl = getJsonString(lookup:actLookup, key: "bbl");
                item.Nta = getJsonString(lookup:actLookup, key: "nta");
                item.Borough = getJsonString(lookup:actLookup, key: "borough");
                results.append(item);
            }
            return results;
        }catch {
            return nil;
        }
    }
    func getJsonString(lookup:[String:Any], key:String) -> String{
        let rawData = lookup[key];
        if rawData != nil{
            let result = rawData as! String;
            return result;
        }
        return "";
    }
}
