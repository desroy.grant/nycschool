//
//  ContentView.swift
//  NYC School Challenge
//
//  Created by Desroy Grant on 4/9/20.
//  Copyright © 2020 Kadd Technology. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
