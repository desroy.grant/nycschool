//
//  DataManager.swift
//  NYC School Challenge
//
//  Created by Desroy Grant on 4/9/20.
//  Copyright © 2020 Kadd Technology. All rights reserved.
//

import Foundation
class DataManager{
    static let shared = DataManager();
    private var _httpDataManager:HttpDataManager?
    private init(){
        self._httpDataManager = HttpDataManager();
    }
    private func processSchoolInfo(schoolData:[School]?,scoreData:[SchoolSatScore]?,completion:@escaping([School]?) -> Void){
        if schoolData == nil {
            completion(nil)
        }
        if scoreData == nil {
            completion(schoolData);
        }
        var lookup = [String:SchoolSatScore]();
        for grd in scoreData! {
            lookup[grd.DBN] = grd;
        }
        
        for s in schoolData! {
            let check = lookup[s.DBN];
            if check == nil {
                s.NumberOfSATTestTakers = "--";
                s.CriticalReadingAvgScore = "--";
                s.MathAvgScore = "--";
                s.WritingAvgScore = "--";
            }else{
                s.NumberOfSATTestTakers = check!.NumberOfSATTestTakers;
                s.CriticalReadingAvgScore = check!.CriticalReadingAvgScore;
                s.MathAvgScore = check!.MathAvgScore;
                s.WritingAvgScore = check!.WritingAvgScore;
            }
        }
        completion(schoolData);
    }
    func GetSchoolInformation(completion:@escaping([School]?) -> Void){
        var schoolData:[School]?
        var scoreData:[SchoolSatScore]?
        var getGradesCompleted = false;
        var getSchoolsCompleted = false;
        
        let onDone = {
            if getSchoolsCompleted && getGradesCompleted{
                self.processSchoolInfo(schoolData: schoolData, scoreData: scoreData, completion: completion)
            }
        }
        let onGetSchoolCompleted = {(schools:[School]?) -> Void in
            getSchoolsCompleted = true;
            schoolData = schools;
            onDone();
        }
        let onGetScoresCompleted = {(scores:[SchoolSatScore]?) -> Void in
            getGradesCompleted = true;
            scoreData = scores;
            onDone();
        }
        self.GetSchools(completion: onGetSchoolCompleted);
        self.GetSchoolScore(completion: onGetScoresCompleted);
    }
    func GetSchools(completion:@escaping([School]?) -> Void) {
        self._httpDataManager?.GetSchools(completion: completion);
    }
    func GetSchoolScore(completion:@escaping([SchoolSatScore]?) -> Void) {
        self._httpDataManager?.GetSchoolScores(completion: completion);
    }
}
