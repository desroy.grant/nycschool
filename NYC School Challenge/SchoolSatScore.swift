//
//  SchoolSatScore.swift
//  NYC School Challenge
//
//  Created by Desroy Grant on 4/9/20.
//  Copyright © 2020 Kadd Technology. All rights reserved.
//


import Foundation
class SchoolSatScore {
    private var _dbn:String!;
    private var _schoolName:String!;
    private var _numberOfSATTestTakers:String!;
    private var _criticalReadingAvgScore:String!;
    private var _mathAvgScore:String!;
    private var _writingAvgScore:String!;
    init(){
    }
    
    var DBN:String{
        get{
            return self._dbn;
        }set{
            self._dbn = newValue;
        }
    }
    var SchoolName:String{
        get{
            return self._schoolName;
        }set{
            self._schoolName = newValue;
        }
    }
    var NumberOfSATTestTakers:String{
        get{
            return self._numberOfSATTestTakers;
        }set{
            self._numberOfSATTestTakers = newValue;
        }
    }
    var CriticalReadingAvgScore:String{
        get{
            return self._criticalReadingAvgScore;
        }set{
            self._criticalReadingAvgScore = newValue;
        }
    }
    var MathAvgScore:String{
        get{
            return self._mathAvgScore;
        }set{
            self._mathAvgScore = newValue;
        }
    }
    var WritingAvgScore:String{
        get{
            return self._writingAvgScore;
        }set{
            self._writingAvgScore = newValue;
        }
    }
}
