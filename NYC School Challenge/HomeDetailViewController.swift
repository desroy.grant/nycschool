//
//  HomeDetailViewController.swift
//  NYC School Challenge
//
//  Created by Desroy Grant on 4/9/20.
//  Copyright © 2020 Kadd Technology. All rights reserved.
//

import Foundation
import UIKit
class HomeDetailViewController : HomeBaseViewController{
    var mainScrollViewControl:UIScrollView!;
    private var _record:School!
    init(record:School) {
        self._record = record;
        super.init(nibName: nil, bundle: nil);
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad();
        self.view.backgroundColor = .white;
        let topY = self.getNavigationBarHeight();
        let scrollHeight = self.view.frame.height - topY;
        var contentHeight = scrollHeight;
        
        self.mainScrollViewControl = UIScrollView(frame: CGRect(x: 0, y: topY, width: self.view.frame.width, height: scrollHeight));
        self.mainScrollViewControl.showsHorizontalScrollIndicator = false;
        self.mainScrollViewControl.alwaysBounceHorizontal = false;
        let valueFont = UIFont(name: "HelveticaNeue", size: 13)!
        
        let padding:CGFloat = 13;
        var startY:CGFloat = 15;
        let controlHeight:CGFloat = 45;
        let verticalSpace:CGFloat = 10;
        let controlWidth:CGFloat = self.view.frame.width - (padding*2);
        
        let nameControl = TitleValueView(title:"Name",value:self._record.Name,frame: CGRect(x: padding, y: startY, width: controlWidth, height: controlHeight));
        self.mainScrollViewControl.addSubview(nameControl);
        startY += controlHeight + verticalSpace;
        
        let satTakesControl = TitleValueView(title:"Number of students who took the SAT",value:self._record.NumberOfSATTestTakers,frame: CGRect(x: padding, y: startY, width: controlWidth, height: controlHeight));
        self.mainScrollViewControl.addSubview(satTakesControl);
        startY += controlHeight + verticalSpace;
        
        let satReadingControl = TitleValueView(title:"SAT Reading Score",value:self._record.CriticalReadingAvgScore,frame: CGRect(x: padding, y: startY, width: controlWidth, height: controlHeight));
        self.mainScrollViewControl.addSubview(satReadingControl);
        startY += controlHeight + verticalSpace;
        
        let satMathControl = TitleValueView(title:"SAT Math Score",value:self._record.MathAvgScore,frame: CGRect(x: padding, y: startY, width: controlWidth, height: controlHeight));
        self.mainScrollViewControl.addSubview(satMathControl);
        startY += controlHeight + verticalSpace;
        
        let satWritingControl = TitleValueView(title:"SAT Writing Score",value:self._record.WritingAvgScore,frame: CGRect(x: padding, y: startY, width: controlWidth, height: controlHeight));
        self.mainScrollViewControl.addSubview(satWritingControl);
        startY += controlHeight + verticalSpace;
        
        let address = self._record.GetFullAddress();
        let addressControl = TitleValueView(title:"Address",value:address,frame: CGRect(x: padding, y: startY, width: controlWidth,
                                                height:55));
        self.mainScrollViewControl.addSubview(addressControl);
        startY += addressControl.frame.height + verticalSpace;
        
        let phoneControl = TitleValueView(title:"Phone",value:self._record.PhoneNumber,frame: CGRect(x: padding, y: startY, width: controlWidth, height: controlHeight));
        self.mainScrollViewControl.addSubview(phoneControl);
        startY += phoneControl.frame.height + verticalSpace;
        
        let faxControl = TitleValueView(title:"Fax",value:self._record.FaxNumber,frame: CGRect(x: padding, y: startY, width: controlWidth, height: controlHeight));
        self.mainScrollViewControl.addSubview(faxControl);
        startY += faxControl.frame.height + verticalSpace;
        
        let emailControl = TitleValueView(title:"Email",value:self._record.Email,frame: CGRect(x: padding, y: startY, width: controlWidth, height: controlHeight));
        self.mainScrollViewControl.addSubview(emailControl);
        startY += emailControl.frame.height + verticalSpace;
        
        let webSiteControl = TitleValueView(title:"Website",value:self._record.Website,frame: CGRect(x: padding, y: startY, width: controlWidth, height: controlHeight));
        self.mainScrollViewControl.addSubview(webSiteControl);
        startY += webSiteControl.frame.height + verticalSpace;
        
        let overviewControl = TitleValueView(title:"Overview",value:self._record.Overview,
                                             frame: CGRect(x: padding, y: startY, width: controlWidth,
                                                height:self.MeasureTextHeight(text: self._record.Overview,
                                                                              font: valueFont,
                                                                              controlWidth: controlWidth)));
        self.mainScrollViewControl.addSubview(overviewControl);
        startY += overviewControl.frame.height + verticalSpace;
        
        let opportunity = String(format:"%@. %@",self._record.Academicopportunities1,self._record.Academicopportunities2);
        let opportunityControl = TitleValueView(title:"Opportunities",value:opportunity,
                                             frame: CGRect(x: padding, y: startY, width: controlWidth,
                                                height:self.MeasureTextHeight(text:opportunity,
                                                                              font: valueFont,
                                                                              controlWidth: controlWidth)));
        self.mainScrollViewControl.addSubview(opportunityControl);
        startY += opportunityControl.frame.height + verticalSpace;
        
        let bottomY = startY + controlHeight + verticalSpace;
        if bottomY > scrollHeight {
            contentHeight = bottomY;
        }else{
            contentHeight = scrollHeight;
        }
        self.view.addSubview(self.mainScrollViewControl);
        mainScrollViewControl.contentSize = CGSize(width: self.view.frame.width, height: contentHeight);
    }
    func MeasureTextHeight(text:String,font:UIFont,controlWidth:CGFloat) -> CGFloat{
        let strObj = NSString(string: text);
        let size:CGSize = strObj.size(withAttributes: [NSAttributedString.Key.font:font]);
        var height = size.width / controlWidth;
        if height < 1 {
            height = 1;
        }else{
            height += 1;
        }
        var fullHeight = height * ceil(font.lineHeight) + 25;
        if fullHeight < 45 {
            fullHeight = 45;
        }
        return fullHeight;
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
        self.title = self._record.Name;
    }
}
