//
//  HttpDataManager.swift
//  NYC School Challenge
//
//  Created by Desroy Grant on 4/9/20.
//  Copyright © 2020 Kadd Technology. All rights reserved.
//

import Foundation
class HttpDataManager {
    init(){
    }
    func GetSchools(completion:@escaping([School]?) -> Void) {
        let allUrl = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json";
        let manager = RequestManager();
        manager.Send(urlPath:allUrl, requestData: nil, requestType:.GET,
                     completion: {(data:Data?) -> Void in
                        let json = JsonDeserializer();
                        let result = json.JsonToSchools(data: data);
                        completion(result)
                        
        }, failure: {(message:String) -> Void in
            completion(nil);
        })
    }
    func GetSchoolScores(completion:@escaping([SchoolSatScore]?) -> Void) {
        let allUrl = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json";
        let manager = RequestManager();
        manager.Send(urlPath:allUrl, requestData: nil, requestType:.GET,
                     completion: {(data:Data?) -> Void in
                        let json = JsonDeserializer();
                        let result = json.JsonToSchoolSATScores(data: data);
                        completion(result)
        }, failure: {(message:String) -> Void in
            completion(nil);
        })
    }
}
