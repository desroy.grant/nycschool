//
//  common.swift
//  NYC School Challenge
//
//  Created by Desroy Grant on 4/9/20.
//  Copyright © 2020 Kadd Technology. All rights reserved.
//

import Foundation
enum RequestType{
    case GET
    case POST
}
