//
//  HomeViewController.swift
//  NYC School Challenge
//
//  Created by Desroy Grant on 4/9/20.
//  Copyright © 2020 Kadd Technology. All rights reserved.
//

import Foundation
import UIKit
class HomeViewController : HomeBaseViewController,UITableViewDelegate,UITableViewDataSource {
    private var _rowHeight:CGFloat = 140;
    private var _records:[School]?;
    var mainTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad();
        let y = self.getNavigationBarHeight() + 10;
        let tableHeight = self.view.frame.height - y;
        self.mainTableView = UITableView()
        self.mainTableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        self.mainTableView.frame = CGRect(x: 0, y: y, width: self.view.frame.width, height:tableHeight)
        self.mainTableView.delegate = self
        self.mainTableView.isScrollEnabled = true;
        self.mainTableView.dataSource = self
        self.mainTableView.cellLayoutMarginsFollowReadableWidth = false;
        self.mainTableView.layoutMargins = UIEdgeInsets.zero;
        self.mainTableView.separatorInset = UIEdgeInsets.zero;
        self.view.addSubview(self.mainTableView);
        self.view.backgroundColor = .white;
        self.mainTableView.backgroundColor = .white;
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if self._records != nil && self._records!.count > 0 {
            return 1;
        }
        return 0;
    }
    func numberOfSections(in tableView: UITableView) -> Int{
        if self._records != nil {
            return self._records!.count;
        }
        return 0;
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 220;
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == self._records!.count - 1 {
            return 0;
        }
        return 1;
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section == self._records!.count - 1 {
            return nil;
        }
        let header = UIView();
        header.backgroundColor = .gray;
        return header;
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let records = self._records!
        let item = records[indexPath.section]
        
        let cellId = "HomeTableViewCell"
        var cell = self.mainTableView.dequeueReusableCell(withIdentifier:cellId) as? HomeTableViewCell
        if cell == nil {
            let nib = UINib(nibName: "HomeTableViewCell", bundle: nil)
            self.mainTableView.register(nib, forCellReuseIdentifier:cellId)
            cell = self.mainTableView.dequeueReusableCell(withIdentifier:cellId) as? HomeTableViewCell
        }
        if cell != nil {
            cell!.setModel(model: item);
        }
        return cell!;
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        self.title = "Schools";
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
        let dm = DataManager.shared;
        dm.GetSchoolInformation(completion: {(records:[School]?) -> Void in
            self._records = records;
            DispatchQueue.main.async {
                self.mainTableView.reloadData();
            }
        });
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) -> Void {
        let item = self._records![indexPath.section];
        let controller = HomeDetailViewController(record: item);
        self.navigationController!.pushViewController(controller, animated:false);
    }
}
