//
//  NYC_School_ChallengeTests.swift
//  NYC School ChallengeTests
//
//  Created by Desroy Grant on 4/9/20.
//  Copyright © 2020 Kadd Technology. All rights reserved.
//

import XCTest
@testable import NYC_School_Challenge

class NYC_School_ChallengeTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testGetSchools() throws {
        let getComplete = {(model:[School]?) -> Void in
            XCTAssert(model != nil);
        }
        
        let dm = DataManager.shared;
        dm.GetSchoolInformation(completion: getComplete);
    }

    func testGetSchoolGrades() throws {
        let getComplete = {(model:[SchoolSatScore]?) -> Void in
            XCTAssert(model != nil);
        }
        
        let dm = DataManager.shared;
        dm.GetSchoolScore(completion: getComplete);
    }

}
